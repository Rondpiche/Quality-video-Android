package com.example.ronpiche.quality_video_android.dummy;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent extends AppCompatActivity {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<DummyItem>();
    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();
    public static final Map<String, Integer> NAME_TO_ID = new HashMap<String, Integer>();

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public  String favName;

        public DummyItem(String favName) {
            this.favName = favName;
            Integer i =NAME_TO_ID.size();
            NAME_TO_ID.put(favName, i);
        }

        @Override
        public String toString() {
            return favName;
        }
    }

    public ArrayList<String> getFavorite(){
        ArrayList<String> favoris = new ArrayList<String>();
        Map<String,?> keys = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> entry : keys.entrySet()){
            if((entry.getValue().toString()=="f")||(entry.getValue().toString()=="f+a")){
                favoris.add(entry.getKey());
            }
        }
        return favoris;
    }
    public ArrayList<String> getAlarm(){
        ArrayList<String> alarm = new ArrayList<String>();
        Map<String,?> keys = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> entry : keys.entrySet()){
            if((entry.getValue().toString() =="f")||(entry.getValue().toString()=="f+a")){
                alarm.add(entry.getKey());
            }
        }
        return alarm;
    }
}
