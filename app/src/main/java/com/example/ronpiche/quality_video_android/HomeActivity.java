package com.example.ronpiche.quality_video_android;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.ronpiche.quality_video_android.dummy.DummyContent;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Julien on 13/11/2016.
 */

public class HomeActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        final View buttonMyFavorite = findViewById(R.id.button_my_favorite);
        buttonMyFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Favorite",Toast.LENGTH_SHORT).show();
                //displayFavoris();
                final Intent favoriteIntent = new Intent(getApplicationContext(), ItemListActivity.class);
                startActivity(favoriteIntent);
            }
        });
        final View buttonNewResearch = findViewById(R.id.button_new_research);
        buttonNewResearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"NewResearch",Toast.LENGTH_SHORT).show();

                final Intent newResearchIntent = new Intent(getApplicationContext(), NewResearchActivity.class);
                startActivity(newResearchIntent);
            }
        });
        final View buttonMyAlarms = findViewById(R.id.button_my_alarms);
        buttonMyAlarms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"MyAlarms",Toast.LENGTH_SHORT).show();
            }
        });
        final View buttonSurpriseMe = findViewById(R.id.button_surprise_me);
        buttonSurpriseMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"SurpriseMe",Toast.LENGTH_SHORT).show();
                List<String> randomSeries = new ArrayList<String>();
                randomSeries.add("game of throne");
                randomSeries.add("black mirror");
                randomSeries.add("walking dead");
                randomSeries.add("once upon a time");

                Context context = v.getContext();
                Intent intent = new Intent(context, ItemDetailActivity.class);//
                Random ran = new Random();
                String s =randomSeries.get(ran.nextInt(randomSeries.size()));
                intent.putExtra(ItemDetailFragment.ARG_ITEM_ID,s);
                //bundle
                intent.putExtra("serieNameKey",s);
                //
                context.startActivity(intent);
            }
        });
    }
    public ArrayList<String> getFavorite(){
        ArrayList<String> favoris = new ArrayList<String>();
        Map<String,?> keys = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> entry : keys.entrySet()){
            if((entry.getValue().toString()=="f")||(entry.getValue().toString()=="f+a")){
                favoris.add(entry.getKey());
            }
        }
        return favoris;
    }
}