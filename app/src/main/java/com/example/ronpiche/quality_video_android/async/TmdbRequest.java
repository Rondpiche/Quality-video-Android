package com.example.ronpiche.quality_video_android.async;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ronpiche.quality_video_android.R;
import com.example.ronpiche.quality_video_android.dummy.DummyContent;
import com.example.ronpiche.quality_video_android.dummy.ResultOfResearch;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Julien on 18/11/2016.
 */

public class TmdbRequest extends AsyncTask<String, Void, Boolean> {
    private ResultOfResearch resultOfResearch = null;
    private TextView overview;
    private TextView name;
    private TextView released;
    private TextView genre;
    private TextView country;
    private TextView language;
    private TextView type;
    private TextView voteAverage;
    private TextView voteCount;
    private Button button_favoris;
    private Button button_alarm;
    private Context context;
    private ImageView imageView;


    public TmdbRequest(TextView overview, TextView name, TextView released, TextView genre, TextView country, TextView language, TextView type, TextView voteAverage, TextView voteCount, Button button_favoris, Button button_alarm, Context applicationContext, ImageView viewById) {
        this.overview = overview;
        this.name = name;
        this.released = released;
        this.genre = genre;
        this.country = country;
        this.language = language;
        this.type = type;
        this.voteAverage = voteAverage;
        this.voteCount = voteCount;
        this.button_favoris = button_favoris;
        this.button_alarm = button_alarm;
        this.context = applicationContext;
        this.imageView = viewById;

    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Boolean doInBackground(String... urls) {
        Log.v("testaaa","tot");
        try {

            OkHttpClient client = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/octet-stream");
            RequestBody body = RequestBody.create(mediaType, "{}");
            Request request = new Request.Builder()
                    .url("https://api.themoviedb.org/3/search/multi?query=" + urls[0] + "&language=en-US&api_key=7249145313439552672367ce12f4d1c3")
                    .get()
                    .build();
            Response response = client.newCall(request).execute();

            String json = response.body().string().toString();
            if (json.contains("total_results\":0,")) {
            } else {
                this.resultOfResearch = new ResultOfResearch(json);
                return true;
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    protected void onPostExecute(Boolean result) {
        if(result) {

            //
            overview.setText(resultOfResearch.getOverview());
            name.setText(resultOfResearch.getName());
            released.setText("released :" + resultOfResearch.getFirstAired());
            genre.setText("genre :" + resultOfResearch.giveAllGenreAsOneString());
            country.setText("country :" + resultOfResearch.getOriginCountry());
            language.setText("language :" + resultOfResearch.getOriginalLanguage());
            type.setText(resultOfResearch.getMediatype());
            voteAverage.setText("voteAverage :" + resultOfResearch.getVoteAverage());
            voteCount.setText("voteCount :" + resultOfResearch.getVoteCount());


            button_favoris.setVisibility(View.VISIBLE);
            button_alarm.setVisibility(View.VISIBLE);


            new DownloadImageTask(this.imageView).execute(resultOfResearch.getPosterPath());
            if (!isAFavorite(resultOfResearch.getName())) {
                button_favoris.setText("ADD TO FAVORITE");
            } else {
                button_favoris.setText("REMOVE TO FAVORITE");
            }
            if (!isAnAlarm(resultOfResearch.getName())) {
                button_alarm.setText("ADD TO ALARM");
            } else {
                button_alarm.setText("REMOVE TO ALARM");
            }

            button_favoris.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (button_favoris.getText() == "REMOVE TO FAVORITE") {
                        button_favoris.setText("ADD TO FAVORITE");
                        removeAsFavorite(resultOfResearch.getName());

                    } else {
                        button_favoris.setText("REMOVE TO FAVORITE");
                        setAsFavorite(resultOfResearch.getName());
                    }
                }
            });
            button_alarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (button_alarm.getText() == "REMOVE TO ALARM") {
                        button_alarm.setText("ADD TO ALARM");
                        removeAsAlarm(resultOfResearch.getName());
                    } else {
                        button_alarm.setText("REMOVE TO ALARM");
                        setAsAlarm(resultOfResearch.getName());
                    }
                }
            });


        }
        /*
        TextView textView = (TextView)view.findViewById(R.id.plot);
        textView.setText(resultOfResearch.getName());*/
    }
    public void setAsFavorite(String serieName){
        String value = returnValue(serieName);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        SharedPreferences.Editor editor = preferences.edit();
        if(value =="a"){
            editor.putString(serieName,"f+a");
        }
        else {
            editor.putString(serieName,"f");
        }
        editor.apply();
        DummyContent.ITEMS.add(new DummyContent.DummyItem(String.valueOf(serieName)));
    }
    public void setAsAlarm(String serieName){
        String value = returnValue(serieName);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        SharedPreferences.Editor editor = preferences.edit();
        if(value =="f"){
            editor.putString(serieName,"f+a");
        }
        else {
            editor.putString(serieName,"a");
        }
        editor.apply();
    }
    public void removeAsFavorite(String serieName){
        String value = returnValue(serieName);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        SharedPreferences.Editor editor = preferences.edit();
        if(value =="f+a"){
            editor.putString(serieName,"a");
        }
        else {
            editor.putString(serieName,"");
        }
        int pos =DummyContent.NAME_TO_ID.get(serieName);
        DummyContent.NAME_TO_ID.remove(serieName);
        DummyContent.ITEMS.remove(pos);
        editor.apply();
    }
    public void removeAsAlarm(String serieName){
        String value = returnValue(serieName);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        SharedPreferences.Editor editor = preferences.edit();
        if(value =="f+a"){
            editor.putString(serieName,"f");
        }
        else {
            editor.putString(serieName,"");
        }
        DummyContent.ITEMS.clear();
        editor.clear();
        editor.apply();
    }

    public String returnValue(String serieName){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        String value = preferences.getString(serieName,"");
        return value;
    }


    public boolean isAFavorite(String serieName){
        String value = returnValue(serieName);
        if(!value.equalsIgnoreCase(""))
        {
            if((value =="f") || (value =="f+a")){
                return  true;
            }
        }
        return false;
    }
    public boolean isAnAlarm(String serieName){
        String value = returnValue(serieName);
        if(!value.equalsIgnoreCase(""))
        {
            if((value =="a") || (value =="f+a")){
                return  true;
            }
        }
        return false;
    }
}
