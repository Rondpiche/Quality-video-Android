package com.example.ronpiche.quality_video_android.dummy;

import android.util.Log;

/**
 * Created by Julien on 17/11/2016.
 */

public class ResultOfResearch {
    private String overview;
    private String voteAverage;
    private String mediatype;
    private String posterPath;
    private String popularity;
    private String id;
    private String firstAired;
    private String originCountry;
    private String originalLanguage;
    private String voteCount;
    private String name;
    private String[] genres;

    public ResultOfResearch(){

    }

    public ResultOfResearch(String json){

        //récupération path image
        String[] jsonFirstSplit = json.split(",\"");
        String[] posterPath = jsonFirstSplit[1].split(":");
        String begin = "https://image.tmdb.org/t/p/w300_and_h450_bestv2";
        posterPath[2] = begin+posterPath[2];
        posterPath[2] = posterPath[2].replace("\\","");
        posterPath[2] = posterPath[2].replace("\"","");
        //Log.v("posterPath",posterPath[2]);
        this.posterPath = posterPath[2];



        //récupération popularité
        String[] popularity = jsonFirstSplit[2].split(":");
        //Log.v("popularity",popularity[1]);
        this.popularity = popularity[1];

        //récupération id
        String[] id = jsonFirstSplit[3].split(":");
        //Log.v("id",id[1]);
        this.id = id[1];

        //récupération overview
        String[] overview = jsonFirstSplit[4].split("\":\"");
        //Log.v("overview",overview[1]);
        this.overview = overview[1];


        //récupération vote average
        String[] voteAverage = jsonFirstSplit[6].split("\":");
        //Log.v("voteAverage",voteAverage[1]);
        this.voteAverage = voteAverage[1];

        //récupération mediaType
        String[] mediatype = jsonFirstSplit[7].split("\":");
        //Log.v("mediaType",mediatype[1]);
        this.mediatype = mediatype[1];

        //récupération firstAiring
        String[] firstAired  = jsonFirstSplit[8].split("\":");
        //Log.v("firstAired",firstAired[1]);
        this.firstAired = firstAired[1];

        //récupération originCountry
        String[] originCountry = jsonFirstSplit[9].split("\":");
        //Log.v("OriginCountry",originCountry[1]);
        this.originCountry = originCountry[1];

        //récupération gender
        String genre = jsonFirstSplit[10].split("\":")[1];
        genre = genre.replace("[","");
        genre = genre.replace("]","");
        String[] allgenre = genre.split(",");
        /*Log.v("lenghAllGender",String.valueOf(allgenre.length));
        Log.v("firstGender",findGenreFromId(Integer.valueOf(allgenre[0])));
        Log.v("secondGender",allgenre[1]);
        Log.v("thirdGender",allgenre[2]);*/
        this.genres = new String[allgenre.length];
        for(int i= 0;i<allgenre.length;i++){
            this.genres[i] = findGenreFromId(Integer.valueOf(allgenre[i]));
        }





        //récupération originalLanguage
        String[] originalLanguage = jsonFirstSplit[11].split("\":");
        //Log.v("originalLanguage",originalLanguage[1]);
        this.originalLanguage = originalLanguage[1];

        //récupération voteCount
        String[] voteCount = jsonFirstSplit[12].split("\":");
        //Log.v("voteCount",voteCount[1]);
        this.voteCount = voteCount[1];


        //récupération name
        String[] name = jsonFirstSplit[13].split("\":");
        //Log.v("name",name[1]);
        this.name = name[1];




    }


    public String findGenreFromId(int i) {
        switch (i) {
            case 10759:
                return "Action & Adventure";
            case 16:
                return "Animation";
            case 35:
                return "Comedy";

            case 80:
                return "Crime";
            case 99:
                return "Documentary";
            case 18:
                return "Drama";
            case 10751:
                return "Family";

            case 10762:
                return "Kids";

            case 9648:
                return "Mistery";

            case 10763:
                return "news";

            case 10764:
                return "Reality";

            case 10765:
                return "Sci-Fi & Fantasy";

            case 10766:
                return "Soap";

            case 10767:
                return "Talk";

            case 10768:
                return "War & Politics";

            case 37:
                return "Westrern";

            case 28:
                return "Action";

            case 12:
                return "Adventure";

            case 14:
                return "Fantasay";

            case 36:
                return "History";

            case 27:
                return "Horror";

            case 10402:
                return "Music";

            case 10749:
                return "Romance";

            case 878:
                return "Science Fiction";

            case 10770:
                return "TV Movie";

            case 53:
                return "Thriller";

            case 10752:
                return "War";
            default:
                return "MissingGenre";

        }

    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(String voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getMediatype() {
        return mediatype;
    }

    public void setMediatype(String mediatype) {
        this.mediatype = mediatype;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstAired() {
        return firstAired;
    }

    public void setFirstAired(String firstAired) {
        this.firstAired = firstAired;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(String voteCount) {
        this.voteCount = voteCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre (int i){
        return this.genres[i];
    }
    public void setGenre (int i, String genre){
        this.genres[i] = genre;
    }
    public String giveAllGenreAsOneString(){
        String result = "";
        for(int i=0;i<this.genres.length;i++){
            result=result+"["+this.genres[i]+"] ";
        }
        return result;
    }

}
