package com.example.ronpiche.quality_video_android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;

import android.widget.TextView;
import android.widget.Toast;

import com.example.ronpiche.quality_video_android.async.DownloadImageTask;
import com.example.ronpiche.quality_video_android.async.TmdbRequest;
import com.example.ronpiche.quality_video_android.dummy.DummyContent;
import com.example.ronpiche.quality_video_android.dummy.ResultOfResearch;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;


/**
 * Created by Julien on 13/11/2016.
 */

public class ItemDetailActivity extends AppCompatActivity {

    public String returnValue(String serieName){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String value = preferences.getString(serieName,"");
        return value;
    }
    public void displaySeriesValue(String serieName){
        String value = returnValue(serieName);
        if(!value.equalsIgnoreCase(""))
        {
            Toast.makeText(getApplicationContext(),value,Toast.LENGTH_SHORT).show();
        }
    }
    public boolean isAFavorite(String serieName){
        String value = returnValue(serieName);
        if(!value.equalsIgnoreCase(""))
        {
            if((value =="f") || (value =="f+a")){
                return  true;
            }
        }
        return false;
    }
    public boolean isAnAlarm(String serieName){
        String value = returnValue(serieName);
        if(!value.equalsIgnoreCase(""))
        {
            if((value =="a") || (value =="f+a")){
                return  true;
            }
        }
        return false;
    }
    public void setAsFavorite(String serieName){
        String value = returnValue(serieName);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        if(value =="a"){
            editor.putString(serieName,"f+a");
        }
        else {
            editor.putString(serieName,"f");
        }
        editor.apply();
        DummyContent.ITEMS.add(new DummyContent.DummyItem(String.valueOf(serieName)));
    }
    public void setAsAlarm(String serieName){
        String value = returnValue(serieName);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        if(value =="f"){
            editor.putString(serieName,"f+a");
        }
        else {
            editor.putString(serieName,"a");
        }
        editor.apply();
    }
    public void removeAsFavorite(String serieName){
        String value = returnValue(serieName);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        if(value =="f+a"){
            editor.putString(serieName,"a");
        }
        else {
            editor.putString(serieName,"");
        }
        int pos =DummyContent.NAME_TO_ID.get(serieName);
        DummyContent.NAME_TO_ID.remove(serieName);
        DummyContent.ITEMS.remove(pos);
        editor.apply();
    }
    public void removeAsAlarm(String serieName){
        String value = returnValue(serieName);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        if(value =="f+a"){
            editor.putString(serieName,"f");
        }
        else {
            editor.putString(serieName,"");
        }
        /*DummyContent.ITEMS.clear();
        editor.clear();*/
        editor.apply();
    }

    public ArrayList<String> getFavorite(){
        ArrayList<String> favoris = new ArrayList<String>();
        Map<String,?> keys = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> entry : keys.entrySet()){
            if((entry.getValue().toString()=="f")||(entry.getValue().toString()=="f+a")){
                favoris.add(entry.getKey());
            }
        }
        return favoris;
    }
    public ArrayList<String> getAlarm(){
        ArrayList<String> alarm = new ArrayList<String>();
        Map<String,?> keys = PreferenceManager.getDefaultSharedPreferences(this).getAll();
        for(Map.Entry<String,?> entry : keys.entrySet()){
            if((entry.getValue().toString() =="f")||(entry.getValue().toString()=="f+a")){
                alarm.add(entry.getKey());
            }
        }
        return alarm;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        final TextView overview = (TextView) findViewById(R.id.plot);
        final TextView name = (TextView) findViewById(R.id.name);
        final TextView released = (TextView) findViewById(R.id.released);
        final TextView genre = (TextView) findViewById(R.id.genre);
        final TextView voteAverage = (TextView) findViewById(R.id.director);
        final TextView voteCount = (TextView) findViewById(R.id.writer);
        final TextView country = (TextView) findViewById(R.id.country);
        final TextView language = (TextView) findViewById(R.id.runtime);
        final TextView type = (TextView) findViewById(R.id.actor);


        final Button button_favoris = (Button) findViewById(R.id.activity_item_detail_button_favoris);
        final Button button_alarm = (Button) findViewById(R.id.activity_item_detail_button_alarm);

        Intent in = getIntent();
        Bundle b = in.getExtras();
        String query =b.getString("serieNameKey");
        TmdbRequest tmdbRequest = new TmdbRequest(overview,name,released,genre,country,language,type,voteAverage,voteCount,button_favoris,button_alarm,getApplicationContext(),(ImageView)findViewById(R.id.activity_item_detail_picture));
        tmdbRequest.execute(query);
    }
}
